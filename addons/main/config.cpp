#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "armaforces_roboto_font_common",
            "armaforces_roboto_font_font"
        };
        author = "veteran29";
        VERSION_CONFIG;
    };
};
