
class RscStructuredText
{
	x=0;
	y=0;
	h=0.035;
	w=0.1;
	text="";
	size="(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	shadow=1;
	class Attributes
	{
		font="RobotoCondensed";
		color="#ffffff";
		colorLink="#D09B43";
		align="left";
		shadow=1;
	};
};

class RscText
{
	x=0;
	y=0;
	h=0.037;
	w=0.30000001;
	style=0;
	shadow=1;
	colorShadow[]={0,0,0,0.5};
	font="RobotoCondensed";
	SizeEx="(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	colorBackground[]={0,0,0,0};
	linespacing=1;
	tooltipColorText[]={1,1,1,1};
	tooltipColorBox[]={1,1,1,1};
	tooltipColorShade[]={0,0,0,0.64999998};
};

class RscLine: RscText
{
	idc=-1;
	style=176;
	x=0.17;
	y=0.47999999;
	w=0.66000003;
	h=0;
	text="";
	colorBackground[]={0,0,0,0};
	colorText[]={1,1,1,1};
};
