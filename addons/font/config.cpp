#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "armaforces_roboto_font_common",
            "ffaa_data",
            "ffaa_ar_bam"
        };
        author = "veteran29";
        VERSION_CONFIG;
    };
};

#include "gui.hpp"
