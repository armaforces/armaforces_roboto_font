name = "ArmaForces - RobotoFont (FFAA Fix)";
action = "https://armaforces.pl";
tooltipOwned = "Roboto is back! (Damn you FFAA!)";
dlcColor[] = {
    0.23,
    0.39,
    0.30,
    1
};
author = "veteran29";
overview = "This small mod makes arma use roboto font. It was created because FFAA makes arma go back to 2016 and use Purista font. -_-";
hideName = 0;
hidePicture = 0;
